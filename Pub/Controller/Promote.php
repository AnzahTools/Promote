<?php


namespace AnzahTools\Promote\Pub\Controller;

use XF\Entity\User;
use XF\Mvc\ParameterBag;

/**
 * Class Promote
 * @package AnzahTools\Promote\Pub\Controller
 */
class Promote extends \XF\Pub\Controller\AbstractController
{
    /**
     * @param $action
     * @param ParameterBag $params
     * @throws \XF\Mvc\Reply\Exception
     */
    protected function preDispatchController($action, ParameterBag $params)
    {
        if (!\XF::visitor()->hasPermission('general', 'at_pdfmt_promoteAllow')) {
            throw $this->exception($this->noPermission());
        }
    }

    public function actionIndex(ParameterBag $params)
    {


        if ($this->isPost()) {
            $input = $this->filter([
                'user_id' => 'uint'
            ]);
            $visitor = \XF::visitor();
            if ($visitor->canPromoteAllow()) {
                $this->promoteUser($visitor);
            }


            return $this->redirect($this->buildLink('members',['user_id' =>$input['user_id']]));

        }
        $mID = $this->filter('mID', 'uint');

        /** @var User $user */
        $user = $this->finder('XF:User')->where(
            [
                'user_id' => $mID
            ]
        )->fetchOne();


        $finder = \XF::finder('XF:UserGroup')->where(
            [
                'at_pdfmt_primaryGroupSelect' => true
            ]
        );
        $primaryGroups = $finder->fetch();
        $finder = \XF::finder('XF:UserGroup')->where(
            [
                'at_pdfmt_secondaryGroupSelect' => true
            ]
        );
        $secondaryGroups = $finder->fetch();

        $primaryGroupIds = [];
        $secondaryGroupIds = [];

        $canChangePrimary = false;

        foreach ($primaryGroups as $group) {

            if ($user['user_group_id'] == $group['user_group_id']) {
                $canChangePrimary = true;
            }

            array_push($primaryGroupIds, [
                '_type' => 'option',
                'value' => $group['user_group_id'],
                'label' => $group['title']
            ]);
        }


        foreach ($secondaryGroups as $group) {

            $temp = false;
            if ($user->isMemberOf($group['user_group_id'])) {
                $temp = true;

            }

            array_push($secondaryGroupIds, [
                'id' => $group['user_group_id'],
                'label' => $group['title'],
                'value' => $temp
            ]);

        }


        $viewParams = [
            'promote_primary_group_ids' => $primaryGroupIds,
            'promote_secondary_group_ids' => $secondaryGroupIds,
            'promote_selected_primary_group' => $user['user_group_id'],
            'promote_can_change_primary' => $canChangePrimary,
            'mID' => $mID

        ];


        return $this->view('AnzahTools/Promote:promote', "at_pdfmt_actions", $viewParams);
    }

    /**
     * @param User $visitor
     * @throws \XF\PrintableException
     */
    protected function promoteUser(User $visitor)
    {


        $input = $this->filter([
            'promote_primary_group' => 'uint',
            'promote_secondary_group' => 'array',
            'user_id' => 'uint'
        ]);

        /** @var User $user */
        $user = $this->finder('XF:User')->where(
            [
                'user_id' => $input['user_id']
            ]
        )->fetchOne();


        if ($visitor->hasPermission("general", "at_pdfmt_promotePrimary")) {
            $finder = \XF::finder('XF:UserGroup')->where(
                [
                    'at_pdfmt_primaryGroupSelect' => true
                ]
            );
            $primaryGroupIds = $finder->pluckFrom('user_group_id')->fetch()->toArray();
            //primary groups are editable and user can promote primary groups
            if (in_array($user->user_group_id, $primaryGroupIds) && in_array($input['promote_primary_group'], $primaryGroupIds) && $visitor->hasPermission("general", "at_pdfmt_promotePrimary")) {
                $user->user_group_id = $input['promote_primary_group'];
                $user->save();
            }
        }

        if ($visitor->hasPermission("general", "at_pdfmt_promoteSecondary")) {
            //secondary groups are editable and visitor can promote secondary groups.
            $finder = \XF::finder('XF:UserGroup')->where(
                [
                    'at_pdfmt_secondaryGroupSelect' => true
                ]
            );
            $secondaryGroupIds = $finder->pluckFrom('user_group_id')->fetch()->toArray();

            $overLapIds = array_intersect($secondaryGroupIds, $user->secondary_group_ids);

            foreach ($overLapIds as $oID) {
                if (isset($input['promote_secondary_group'][$oID])) {
                    if ($input['promote_secondary_group'][$oID] == "1") {

                    } else {
                        $user->removeUserFromGroup($oID);

                    }

                } else {
                    $user->removeUserFromGroup($oID);
                }


            }



            $ids = $user->secondary_group_ids;
            foreach ($secondaryGroupIds as $oID) {
                if (isset($input['promote_secondary_group'][$oID])) {
                    if ($input['promote_secondary_group'][$oID] == "1" && !in_array($oID, $user->secondary_group_ids)) {
                        array_push($ids, $oID);
                    }
                }
            }
            $user->secondary_group_ids = $ids;

        }

        $user->save();
        $user->rebuildDisplayStyleGroup();
        $user->rebuildPermissionCombination();
        $user->rebuildUserGroupRelations();


    }

}
