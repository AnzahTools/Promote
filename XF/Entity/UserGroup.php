<?php

namespace AnzahTools\Promote\XF\Entity;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * @property bool at_promote_primaryGroupSelect
 * @property bool at_promote_secondaryGroupSelect
 */
class UserGroup extends XFCP_UserGroup
{

    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns['at_pdfmt_primaryGroupSelect'] = ['type' => Entity::BOOL, 'default' => 0];
        $structure->columns['at_pdfmt_secondaryGroupSelect'] = ['type' => Entity::BOOL, 'default' => 0];

        return $structure;
    }
}
