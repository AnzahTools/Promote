<?php


namespace AnzahTools\Promote\XF\Entity;


/**
 * Class User
 * @package AnzahTools\Promote\XF\Entity
 */
class User extends XFCP_User
{
    /**
     * @return bool
     */
    public function canPromoteAllow ()
    {
        return $this->hasPermission('general', 'at_pdfmt_promoteAllow');
    }
    /**
     * @return bool
     */
    public function canPromotePrimaryGroup ()
    {
        return $this->hasPermission('general', 'at_pdfmt_promotePrimary');
    }
    /**
     * @return bool
     */
    public function canPromoteSecondaryGroup()
    {
        return $this->hasPermission('general', 'at_pdfmt_promoteSecondary');
    }
    /**
     * @return bool
     */
    public function canPromoteStaff()
    {
        return $this->hasPermission('general', 'at_pdfmt_promoteStaff');
    }

}
