<?php

namespace AnzahTools\Promote\XF\Admin\Controller;

/**
 * Class UserGroup
 * @package AnzahTools\Promote\XF\Admin\Controller
 */
class UserGroup extends XFCP_UserGroup
{
    protected function userGroupSaveProcess(\XF\Entity\UserGroup $userGroup)
    {
        $form = parent::userGroupSaveProcess($userGroup);

        $input = $this->filter(
            [
                'at_pdfmt_primaryGroupSelect' => 'bool',
                'at_pdfmt_secondaryGroupSelect'     => 'bool'
            ]
        );

        $form->setup(
            function () use ($userGroup, $input) {
                $userGroup->bulkSet($input);
            }
        );

        return $form;
    }
}