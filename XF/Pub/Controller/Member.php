<?php


namespace AnzahTools\Promote\XF\Pub\Controller;

use XF\Mvc\ParameterBag;
//use XFRM\XF\Entity\XFCP_Member;

/**
 * Class Member
 * @package AnzahTools\Promote\XF\Pub\Controller
 */
class Member extends XFCP_Member
{

    public function actionIndex(ParameterBag $params)
    {
        $response = parent::actionIndex($params);

        if (!$response instanceof \XF\Mvc\Reply\View) {
            return $response;
        }

        /** @var \XF\Entity\User $visitor */
        $visitor = \XF::visitor();
        /** @var \XF\Entity\Option $options */
        $options = \XF::options();

        $canPromote = $visitor->hasPermission('general', 'at_pdfmt_promoteAllow');
        $canPromotePrimary = $visitor->hasPermission('general', 'at_pdfmt_promotePrimary');
        $canPromoteSecondary = $visitor->hasPermission('general', 'at_pdfmt_promoteSecondary');
        $canPromoteStaff = $visitor->hasPermission('general', 'at_pdfmt_promoteStaff');


        if($canPromote)
        {
            $response->setParam('canPromote', true);
        }else{
            $response->setParam('canPromote', false);
        }






        return $response;
    }

}
