<?php

// ################## THIS IS A GENERATED FILE ##################
// DO NOT EDIT DIRECTLY. EDIT THE CLASS EXTENSIONS IN THE CONTROL PANEL.

namespace AnzahTools\Promote\XF\Admin\Controller
{
	class XFCP_UserGroup extends \XF\Admin\Controller\UserGroup {}
}

namespace AnzahTools\Promote\XF\Entity
{
	class XFCP_User extends \XF\Entity\User {}
	class XFCP_UserGroup extends \XF\Entity\UserGroup {}
}

namespace AnzahTools\Promote\XF\Pub\Controller
{
	class XFCP_Member extends \XF\Pub\Controller\Member {}
}